import { readFileSync } from "fs";
import { createServer } from "https";

import { v4 } from "uuid";
import WebSocket from "./modules/ws/index.js";

const PORT = 2808;
var webServer = createServer( {
	"key": readFileSync( "/etc/letsencrypt/live/my-dirty-socks.rockerest.xyz/privkey.pem" ),
	"cert": readFileSync( "/etc/letsencrypt/live/my-dirty-socks.rockerest.xyz/fullchain.pem" )
} );
var socketServer = new WebSocket.Server({ "server": webServer });
var sockets = {};
var pingingInterval;

function rcv( msg ){
	console.log( this.id, "said:", msg );
	Object
		.values( sockets )
		.forEach( ( sock ) => {
			if( sock.id != this.id ){
				sock.send( JSON.stringify( {
					"name": "message",
					"type": "text",
					"data": msg
				} ) );
			}
		} );
}

pingingInterval = setInterval( () => {
	Object
		.values( sockets )
		.forEach( ( socket ) => {
			if( !socket.alive ){
				console.log( socket.id, "heart attack" );
				socket.terminate();
			}
			else{
				socket.ping(() => {});
			}
		} );
}, 30000 );

socketServer.on( "connection", ( socket ) => {
	let id = v4();

	console.log( id, "joined" );

	socket.id = id;
	socket.alive = true;

	sockets[ id ] = socket;

	socket.on( "message", rcv.bind( socket ) );
	socket.on( "pong", () => socket.alive = true );
	socket.on( "close", () => {
		console.log( "removing", id );
		delete sockets[ id ];
	} );
} );

socketServer.on( "close", () => {
	console.log( "shutting down the socket server" );
	if( pingingInterval ){
		clearInterval( pingingInterval );
	}
} );

console.log( `The server is starting on :${PORT}` );

webServer.listen( PORT );