import { b as bufferutil } from './common/index-4466b5a6.js';
export { b as __moduleExports, b as default } from './common/index-4466b5a6.js';
import './common/index-9878d1ec.js';
import 'fs';
import 'path';
import 'os';



var mask = bufferutil.mask;
var unmask = bufferutil.unmask;
export { mask, unmask };
