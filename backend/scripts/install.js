import { install } from "esinstall";

await install(
	[
		"ws/index.js",
		"utf-8-validate",
		"bufferutil"
	],
	{
		"dest": "modules",
		"treeshake": true,
		"external": [
			"events",
			"stream",
			"http",
			"https",
			"crypto",
			"path",
			"zlib",
			"tls",
			"net",
			"url",
			"os",
			"fs"
		]
	}
);